module.exports = {
    plugins: ['@typescript-eslint'],
    extends: ['@solent/eslint-config', 'plugin:@typescript-eslint/recommended'],
    rules: {
        /* typescript plugin rules */
        'no-extra-parens': 0,
        '@typescript-eslint/no-extra-parens': 2,
        'no-duplicate-imports': 0,
        '@typescript-eslint/no-duplicate-imports': 2,
        '@typescript-eslint/typedef': 1,
        '@typescript-eslint/type-annotation-spacing': 2,
        '@typescript-eslint/no-unnecessary-type-arguments': 2,
        '@typescript-eslint/no-unnecessary-condition': 2,
        '@typescript-eslint/no-type-alias': [1, { allowAliases: "in-unions", allowTupleTypes: "in-unions" }],
        'no-magic-numbers': 0,
        '@typescript-eslint/no-magic-numbers': [1, { ignore: [-1, 0, 1, 2], ignoreDefaultValues: true, ignoreEnums: true }],
        'no-dupe-class-members': 0,
        '@typescript-eslint/no-dupe-class-members': 1,
        'no-array-constructor': 0,
        '@typescript-eslint/no-array-constructor': 2,
        'brace-style': 0,
        '@typescript-eslint/brace-style': 2,
        'no-invalid-this': 0,
        '@typescript-eslint/no-invalid-this': 2
    }
};
