# Solent eslint config - with TypeScript

Configuration for ESLint with linting settings for TypeScript using [@typescript-eslint/eslint-plugin](https://www.npmjs.com/package/@typescript-eslint/eslint-plugin). This configuration extends our base eslint configuration [@solent/eslint-config](https://www.npmjs.com/package/@solent/eslint-config) and the recommended settings from `@typescript-eslint/eslint-plugin`.

## Peer dependencies

- [@typescript-eslint/eslint-plugin](https://www.npmjs.com/package/@typescript-eslint/eslint-plugin)
- [eslint](https://www.npmjs.com/package/eslint)